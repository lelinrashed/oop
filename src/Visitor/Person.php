<?php 
	
	namespace rashed\Visitor;

	class Person{
		public $first_name = "";
		public $last_name = "";

		public function __construct($fname, $lname)
		{
			$this->first_name = $fname;
			$this->last_name = $lname;
		}

		public function __toString()
		{
			return $this->first_name." and ".$this->last_name."<br>";
		}
	}



?>